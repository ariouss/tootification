#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2019-2021

import config
import gi
gi.require_versions({'Notify': '0.7', 'GdkPixbuf': '2.0'})
from bs4 import BeautifulSoup as BS
from gi.repository import Notify, GdkPixbuf
from mastodon import Mastodon
from time import sleep
from urllib import request


def connect():
    mastodon = Mastodon(
            access_token = config.access_token,
            api_base_url = config.most_inst
            )
    return mastodon

def get_toot_text(toot):
    content = BS(toot['content'],
            features='html.parser')
    text = content.get_text()
    return text

def get_tooter_name(toot):
    tooter = toot['account']['display_name']
    if tooter == '':
        tooter = toot['account']['username']
    return tooter

def get_tooter_avatar(toot):
    avatar_address = toot['account']['avatar']
    try:
        user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
        headers = {'User-Agent':user_agent,}
        req = request.Request(avatar_address, None, headers)
        response = request.urlopen(req, timeout=3)
        loader = GdkPixbuf.PixbufLoader()
        loader.write(response.read())
        loader.close()
        loader = loader.get_pixbuf()
        avatar = loader
    except Exception as e:
        avatar = GdkPixbuf.Pixbuf.new_from_file('missing.png')
    return avatar

def create_notification(toot):
    text = get_toot_text(toot)
    tooter = get_tooter_name(toot)
    notification = Notify.Notification.new(
            tooter, text)
    avatar = get_tooter_avatar(toot)
    notification.set_image_from_pixbuf(avatar)
    # to show expanded and can be displayed in fullscreen
    notification.set_urgency(2)
    notification.set_timeout(Notify.EXPIRES_DEFAULT)
    return notification

def show_and_close(notification):
    notification.show()
    #TODO: think for a better way
    # we should close notifications cause they're urgent
    # Urgent notifications don't have timeout in gnome
    sleep(config.show_time)
    notification.close()

def get_favourites(mastodon):
    favourites = mastodon.favourites()
    stack = []
    while favourites:
        for toot in favourites:
            stack.append(toot)
        favourites = mastodon.fetch_next(favourites)
    return stack

def main():
    mastodon = connect()
    Notify.init(config.apps_name)
    while True:
        favourites = get_favourites(mastodon)
        for iteration in range(len(favourites)):
            toot = favourites.pop()
            notification = create_notification(toot)
            show_and_close(notification)
            mastodon.status_unfavourite(toot['id'])
        #TODO: think for a better way
        sleep(config.intr_time)
    Notify.uninit()

if __name__ == '__main__':
    main()
