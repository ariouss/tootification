#!/usr/bin/python3
# Released under GPLv3+ License
# Danial Behazdi <dani.behzi@ubuntu.com>, 2019-2021

from mastodon import Mastodon
import config

Mastodon.create_app(
        config.apps_name,
        api_base_url = config.most_inst,
        to_file = config.client_creds
        )

mastodon = Mastodon(
        client_id = config.client_creds,
        api_base_url = config.most_inst
        )

mastodon.log_in(
        config.user_name,
        config.user_pass,
        to_file = config.access_token
        )
